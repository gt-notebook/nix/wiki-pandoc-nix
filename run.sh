#!/bin/bash

if [ -f .env ]; then
export $(grep -v '#.*' .env | xargs)
fi

export PATH=$PATH:.
echo $PANDOC

./my-script process wiki/notes/
./my-script build wiki/notes_converted/
