{
  description = "PandocProject";
  inputs.haskell-nix.url = "github:input-output-hk/haskell.nix";
  inputs.nixpkgs.follows = "haskell-nix/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.wiki-python.url = "git+https://gitlab.huma-num.fr/gt-notebook/nix/wiki-script.git?ref=master";
  inputs.wiki-pandoc.url = "git+https://gitlab.huma-num.fr/gt-notebook/nix/pandoc-2-14-02.git?ref=main";

  outputs = { self, nixpkgs, flake-utils, wiki-python, haskell-nix,  wiki-pandoc }:
      let
      pkgsForSystem = system:
        (import nixpkgs) {
          inherit wiki-pandoc;
          inherit wiki-python;
          inherit system;
      };

      in (flake-utils.lib.eachSystem ["x86_64-linux"] (system: rec {

     myPackage = pkgsForSystem system;

     devShell =
            myPackage.mkShell { packages = [
            myPackage.graphviz
            myPackage.wget
            myPackage.bash
            myPackage.findutils
            ]; };

       defaultPackage = myPackage.runCommand "combined" {
       } ''
      mkdir -p $out/bin
      cp -rs ${wiki-python.defaultPackage.x86_64-linux}/bin/* $out/bin
      cp -rs ${wiki-pandoc.defaultPackage.x86_64-linux}/bin/* $out/bin
    '';
  }));
    }

